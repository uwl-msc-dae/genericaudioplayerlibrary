
#pragma once

#include <JuceHeader.h>

using namespace juce;
using namespace std;

class WaveformComponent    : public Component,
                             private ChangeListener
{
public:
    WaveformComponent(int sourceSamplesPerThumbnailSample, AudioFormatManager& formatManager, AudioThumbnailCache& cache);
    ~WaveformComponent();
    
    void setFile (const File& file);
    void clear();

    void paint (Graphics& g) override;
    void paintIfNoFileLoaded (Graphics& g);
    void paintIfFileLoaded (Graphics& g);
    
    void changeListenerCallback (ChangeBroadcaster* source) override;
    
    void resized() override;

private:
    AudioThumbnail waveform;
    void waveformChanged();
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (WaveformComponent)
};
