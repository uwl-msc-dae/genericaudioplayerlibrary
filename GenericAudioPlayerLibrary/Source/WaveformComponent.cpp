
#include "WaveformComponent.h"

WaveformComponent::WaveformComponent(int sourceSamplesPerThumbnailSample, AudioFormatManager& formatManager, AudioThumbnailCache& cache) : waveform(sourceSamplesPerThumbnailSample, formatManager, cache)
{
    waveform.addChangeListener (this);
}

WaveformComponent::~WaveformComponent()
{
}

void WaveformComponent::setFile (const File& file)
{
    waveform.setSource(new FileInputSource (file));
}

void WaveformComponent::clear()
{
    waveform.setSource(nullptr);
}

void WaveformComponent::waveformChanged()
{
    repaint();
}

void WaveformComponent::paint (Graphics& g)
{
    if (waveform.getNumChannels() == 0)
        paintIfNoFileLoaded (g);
    else
        paintIfFileLoaded (g);
}

void WaveformComponent::paintIfNoFileLoaded (Graphics& g)
{
    g.setColour(Colours::black);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 20);
    
    g.setColour (Colours::darkgoldenrod);
    g.drawFittedText ("No File Loaded", getLocalBounds(), Justification::centred, 1);
}

void WaveformComponent::paintIfFileLoaded (Graphics& g)
{
    g.setColour(Colours::black);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(), 20);

    g.setColour (Colours::darkgoldenrod);
    waveform.drawChannels (g, getLocalBounds(), 0.0, waveform.getTotalLength(), 1.0f);
}

void WaveformComponent::changeListenerCallback (ChangeBroadcaster* source)
{
    if (source == &waveform)
        waveformChanged();
}

void WaveformComponent::resized()
{
}
