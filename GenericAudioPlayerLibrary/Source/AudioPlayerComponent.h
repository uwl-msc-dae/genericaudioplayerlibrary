
#pragma once

#include "WaveformComponent.h"
#include "PlayheadOverlay.h"

using namespace juce;
using namespace std;

class AudioPlayerComponent    : public AudioAppComponent,
                                public ChangeListener
{
public:
    AudioPlayerComponent();
    ~AudioPlayerComponent();
    
    void prepareToPlay (int samplesPerBlockExpected, double sampleRate) override;
    void getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill) override;
    void releaseResources() override;

    void paint (Graphics&) override;
    void resized() override;

private:
    enum TransportState
    {
        Stopped,
        StartResume,
        Stopping,
        Playing,
        Pausing,
        Unloading
    };
    
    TransportState currentState;
    
    TextButton openButton;
    TextButton playButton;
    TextButton pauseButton;
    TextButton stopButton;
    TextButton clearButton;
    
    void openButtonClicked();
    void playButtonClicked();
    void pauseButtonClicked();
    void stopButtonClicked();
    void clearButtonClicked();
    
    void transportStateChanged(TransportState newState);
    void changeListenerCallback(ChangeBroadcaster *source) override; //override signifies 'virtual' function
    
    AudioTransportSource transport;
    AudioFormatManager formatManager;
    
    std::unique_ptr<AudioFormatReaderSource> playSource;
    
    AudioThumbnailCache waveformCache;
    WaveformComponent waveformComp;
    PlayheadOverlay playhead;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioPlayerComponent)
};
