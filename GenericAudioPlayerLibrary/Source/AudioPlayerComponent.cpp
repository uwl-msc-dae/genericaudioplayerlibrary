
#include "AudioPlayerComponent.h"

AudioPlayerComponent::AudioPlayerComponent() : currentState(Stopped), openButton("load"), playButton("play"), pauseButton("pause"), stopButton("stop"), clearButton("unload"), waveformCache (5), waveformComp (512, formatManager, waveformCache), playhead(transport)
{
    setSize(getWidth() - 20, getHeight() - 340);
    
    // Some platforms require permissions to open input channels so request that here
    if (RuntimePermissions::isRequired (RuntimePermissions::recordAudio)
        && ! RuntimePermissions::isGranted (RuntimePermissions::recordAudio))
    {
        RuntimePermissions::request (RuntimePermissions::recordAudio,
                                     [&] (bool granted) { if (granted)  setAudioChannels (2, 2); });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels (2, 2);
    }
    
    openButton.onClick = [this] {openButtonClicked();};
    openButton.setColour(TextButton::buttonColourId, Colours::black);
    openButton.setEnabled(true);
    addAndMakeVisible(&openButton);
    
    playButton.onClick = [this] {playButtonClicked();};
    playButton.setColour(TextButton::buttonColourId, Colours::green);
    playButton.setEnabled(false);
    addAndMakeVisible(&playButton);
    
    pauseButton.onClick = [this] {pauseButtonClicked();};
    pauseButton.setColour(TextButton::buttonColourId, Colours::blue);
    pauseButton.setEnabled(false);
    addAndMakeVisible(&pauseButton);
    
    stopButton.onClick = [this] {stopButtonClicked();};
    stopButton.setColour(TextButton::buttonColourId, Colours::orangered);
    stopButton.setEnabled(false);
    addAndMakeVisible(&stopButton);
    
    clearButton.onClick = [this] {clearButtonClicked();};
    clearButton.setColour(TextButton::buttonColourId, Colours::rebeccapurple);
    clearButton.setEnabled(false);
    addAndMakeVisible(&clearButton);
    
    formatManager.registerBasicFormats();
    transport.addChangeListener(this);
    
    addAndMakeVisible(&waveformComp);
    addChildComponent(&playhead);
    playhead.setVisible(false);
}

AudioPlayerComponent::~AudioPlayerComponent()
{
    shutdownAudio();
}

void AudioPlayerComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    transport.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void AudioPlayerComponent::openButtonClicked()
{
    FileChooser chooser("Choose an Audio File (wav or aiff)", File::getSpecialLocation(File::userDesktopDirectory), "*.wav; *.mp3; *.m4a");
    
    if (chooser.browseForFileToOpen())
    {
        auto file = chooser.getResult();
        auto * reader = formatManager.createReaderFor(file);
        
        if (reader != nullptr)
        {
            std::unique_ptr<AudioFormatReaderSource> tempSource(std::make_unique <AudioFormatReaderSource>(reader, true));
            transport.setSource(tempSource.get(), 0, nullptr, reader->sampleRate);
            playSource.reset(tempSource.release());
            
            waveformComp.setFile(file);
            playhead.setVisible(true);

            transportStateChanged(Stopped);
            
            playButton.setEnabled(true);
            clearButton.setEnabled(true);
            openButton.setEnabled(false);
        }
    }
}

void AudioPlayerComponent::playButtonClicked()
{
    transportStateChanged(StartResume);
}

void AudioPlayerComponent::pauseButtonClicked()
{
    transportStateChanged(Pausing);
}

void AudioPlayerComponent::stopButtonClicked()
{
    transportStateChanged(Stopping);
}

void AudioPlayerComponent::clearButtonClicked()
{
    transportStateChanged(Unloading);
}

void AudioPlayerComponent::transportStateChanged(TransportState newState)
{
    if (newState != currentState)
    {
        currentState = newState;
        
        switch (currentState) {
            case Stopped:
                transport.stop();
                transport.setPosition(0.0);
                break;
                
            case Playing:
                break;
                
            case StartResume:
                playButton.setEnabled(false);
                pauseButton.setEnabled(true);
                stopButton.setEnabled(true);
                clearButton.setEnabled(false);
                
                transport.start();
                break;
            
            case Stopping:
                playButton.setEnabled(true);
                pauseButton.setEnabled(false);
                stopButton.setEnabled(false);
                clearButton.setEnabled(true);
                
                transport.stop();
                transport.setPosition(0.0);
                break;
            
            case Pausing:
                playButton.setEnabled(true);
                pauseButton.setEnabled(false);
                stopButton.setEnabled(true);
                clearButton.setEnabled(true);
                
                transport.stop();
                break;
                
            case Unloading:
                playButton.setEnabled(false);
                pauseButton.setEnabled(false);
                stopButton.setEnabled(false);
                
                clearButton.setEnabled(false);
                openButton.setEnabled(true);
                
                waveformComp.clear();
                playhead.setVisible(false);
                break;
        }
    }
}

void AudioPlayerComponent::changeListenerCallback(ChangeBroadcaster *source)
{
    if (source == &transport)
    {
        if (transport.isPlaying())
        {
            transportStateChanged(Playing);
        }
        else
        {
            if (transport.getCurrentPosition() < transport.getLengthInSeconds())
            {
                return;
            }
            
            transportStateChanged(Stopping);
        }
    }
}

void AudioPlayerComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    if(playSource == nullptr)
    {
        bufferToFill.clearActiveBufferRegion();
        return;
    }
    
    transport.getNextAudioBlock(bufferToFill);
}

void AudioPlayerComponent::releaseResources()
{
    transport.setSource(nullptr);
    playSource.reset(nullptr);
}

void AudioPlayerComponent::paint (Graphics& g)
{
    g.setColour(Colours::darkgoldenrod);
    g.fillRoundedRectangle(0, 0, getWidth(), 230, 20);
}

void AudioPlayerComponent::resized()
{
    openButton.setBounds(10, 10, getWidth() - 20, 30);
    playButton.setBounds(10, 55, getWidth() - 20, 30);
    pauseButton.setBounds(10, 100, getWidth() - 20, 30);
    stopButton.setBounds(10, 145, getWidth() - 20, 30);
    clearButton.setBounds(10, 190, getWidth() - 20, 30);
    
    Rectangle<int> waveformBounds (0, 230, getWidth(), getHeight() - 230);
    waveformComp.setBounds (waveformBounds);
    playhead.setBounds (waveformBounds);
}
