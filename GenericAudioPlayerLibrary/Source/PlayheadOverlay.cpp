
#include "../JuceLibraryCode/JuceHeader.h"
#include "PlayheadOverlay.h"

PlayheadOverlay::PlayheadOverlay(AudioTransportSource& transportSourceToUse)
                                         : transportSource (transportSourceToUse)
{
    startTimer (40);
}

PlayheadOverlay::~PlayheadOverlay()
{
}

void PlayheadOverlay::mouseDown (const MouseEvent& event)
{
    auto duration = transportSource.getLengthInSeconds();

    if (duration > 0.0)
    {
        auto clickPosition = event.position.x;
        auto audioPosition = (clickPosition / getWidth()) * duration;

        transportSource.setPosition (audioPosition);
    }
}

void PlayheadOverlay::timerCallback()
{
    repaint();
}

void PlayheadOverlay::paint (Graphics& g)
{
    auto duration = (float) transportSource.getLengthInSeconds();

    if (duration > 0.0)
    {
        auto audioPosition = (float) transportSource.getCurrentPosition();
        auto drawPosition = (audioPosition / duration) * getWidth();

        g.setColour (Colours::green);
        g.drawLine (drawPosition, 15.0f, drawPosition, (float) getHeight() - 15.0f, 2.0f);
    }
}

void PlayheadOverlay::resized()
{
}
