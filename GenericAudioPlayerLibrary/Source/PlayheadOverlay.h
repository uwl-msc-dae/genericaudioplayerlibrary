
#pragma once

#include <JuceHeader.h>

using namespace juce;
using namespace std;

class PlayheadOverlay    : public Component,
                           private Timer
{
public:
    PlayheadOverlay(AudioTransportSource& transportSourceToUse);
    ~PlayheadOverlay();
    
    void mouseDown (const MouseEvent& event) override;

    void paint (Graphics&) override;
    void resized() override;

private:
    AudioTransportSource& transportSource;
    void timerCallback() override;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PlayheadOverlay)
};
